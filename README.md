﻿[TOC]
**********************************************************

# Introduction
>Spring Boot
>Spring Security
>Hibernate
>Thymeleaf

# How to setup

- checkout source code
>git clone https://hsjeon70@bitbucket.org/hsjeon70/jlook.micro.sso.git

- generate eclipse-related files
>gradle eclipse

- run program
>gradle run

- testing
> http://localhost:9090/


