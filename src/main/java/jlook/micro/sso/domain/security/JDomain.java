package jlook.micro.sso.domain.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import jlook.micro.sso.domain.JBaseEntity;

@Entity
@Table(name="JDOMAIN")
public class JDomain extends JBaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="NAME")
	private String name;
	@Column(name="DESCRIPTION")
	private String description;
	@Column(name="ACTIVE")
	private boolean active;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
}
