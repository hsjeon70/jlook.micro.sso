package jlook.micro.sso.infrastructure.metadata;

import jlook.micro.sso.domain.security.JDomain;

public enum JDomainDef {
	SYSTEM(JDomainDef.SYSTEM_ID, JDomainDef.SYSTEM_ID, "SYSTEM", "System Common Domain");
	
	JDomainDef(Long objectId, Long domainId, String name, String description) {
		this.instance = new JDomain();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setName(name);
		this.instance.setDescription(description);
		this.instance.setActive(true);
	}
	
	public JDomain instance;

	public static final long SYSTEM_ID=1L;
}
