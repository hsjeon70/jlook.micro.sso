package jlook.micro.sso.infrastructure.metadata;

import java.util.HashSet;
import java.util.Set;

import jlook.micro.sso.domain.security.JUser;
import jlook.micro.sso.domain.security.JRole;

public enum JUserDef {
	admin(1L, JDomainDef.SYSTEM.instance.getObjectId(), "hsjeon70", "1q2w3e4r", "hsjeon70@gmail.com","HongSeong", "Jeon", JRoleDef.sysadmin.instance);
	
	JUserDef(Long objectId, Long domainId, String username, String password, String email, String firstName, String lastName, JRole jRole) {
		this.instance = new JUser();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(domainId);
		this.instance.setUsername(username);
		this.instance.setPassword(password);
		this.instance.setFirstName(firstName);
		this.instance.setLastName(lastName);
		this.instance.setEmail(email);
		
		Set<JRole> roles = new HashSet<JRole>();
		roles.add(JRoleDef.user.instance);
		if(jRole!=null) {
			roles.add(jRole);
		}
		this.instance.setRoles(roles);
	}
	
	public JUser instance;
}
