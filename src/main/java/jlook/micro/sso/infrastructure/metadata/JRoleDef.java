package jlook.micro.sso.infrastructure.metadata;

import jlook.micro.sso.domain.security.JRole;
import jlook.micro.sso.infrastructure.security.JRoleType;

public enum JRoleDef {
	user(JRoleDef.USER_ID, JRoleType.user, "General user"),
	sysadmin(JRoleDef.SYSADMIN_ID, JRoleType.sysadmin, "System Administrator"),
	admin(JRoleDef.ADMIN_ID, JRoleType.admin, "Domain Administrator");
	
	JRoleDef(Long objectId, JRoleType roleType, String description) {
		this.instance = new JRole();
		this.instance.setObjectId(objectId);
		this.instance.setDomainId(JDomainDef.SYSTEM.instance.getDomainId());
		this.instance.setName(roleType.getName());
		this.instance.setDescription(description);
	}
	public JRole instance;
	
	public static final long USER_ID=1L;
	public static final long SYSADMIN_ID=2L;
	public static final long ADMIN_ID=3L;
}
