package jlook.micro.sso.infrastructure.jdbc;

import java.sql.Types;

import org.hibernate.dialect.H2Dialect;

public class JH2Dialect extends H2Dialect {
	
	public JH2Dialect() {
		super();
		registerHibernateType(Types.BOOLEAN, "boolean");
	}
}
