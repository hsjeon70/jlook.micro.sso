package jlook.micro.sso.infrastructure;

import jlook.micro.sso.service.JServiceException;

public interface JLifecycle {
	
	public void startup() throws JServiceException;
	
	public void destory() throws JServiceException;
	
}
