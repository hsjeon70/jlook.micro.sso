package jlook.micro.sso.infrastructure;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import jlook.micro.sso.infrastructure.annotation.Message;

@Message(text="Application Error is occured.")
public class JException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected List<Object> parameters = new ArrayList<Object>();
	
	public JException(String message) {
		this(message, null, null);
	}
	
	public JException(Object[] parameters) {
		this(null, null, parameters);
	}
	
	public JException(String message, Object[] parameters) {
		this(message, null, parameters);
	}

	public JException(Throwable cause, Object[] parameters) {
		this(null, cause, parameters);
	}
	
	public JException(String message, Throwable cause) {
		this(message, cause, null);
	}
	
	public JException(String message, Throwable cause, Object[] parameters) {
		super(message, cause);
		if(parameters==null) return;
		for(Object param : parameters) {
			this.parameters.add(param);
		}
	}
	
	public String getMessage() {
		String msg = super.getMessage();
		if(msg==null) {
			return "";
		}
		
		if(this.parameters==null || this.parameters.size()==0) {
			return msg;
		}
		
		return MessageFormat.format(msg, this.parameters.toArray());
	}
	
	public String toString() {
		String name = getClass().getSimpleName();
		if(name.charAt(0)=='J') {
			name = name.substring(1);
		}
		
		if(name.endsWith("Exception")) {
			name = name.substring(0,name.length()-"Exception".length());
		}
		Package pkg = getClass().getPackage();
		String pkgName = pkg.getName();
		
		int idx = pkgName.lastIndexOf(".");
		String layer = pkgName.substring(idx+1);
		return name+"Error("+layer+") - "+getMessage();
	}
}
