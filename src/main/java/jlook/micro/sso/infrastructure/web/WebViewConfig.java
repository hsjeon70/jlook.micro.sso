package jlook.micro.sso.infrastructure.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebViewConfig extends WebMvcConfigurerAdapter {
	
	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/hello").setViewName("hello");
        registry.addViewController("/test").setViewName("mytest");
        registry.addViewController("/login").setViewName("login");
        
        registry.addViewController("/welcome").setViewName("welcome");
        registry.addViewController("/excepition").setViewName("excepition");
        registry.addViewController("/owners/find").setViewName("owners/findOwners");
        //registry.addViewController("/fragments/bodyHeader").setViewName("fragments/bodyHeader");
        
        
    }
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/resources/");
	}
}
