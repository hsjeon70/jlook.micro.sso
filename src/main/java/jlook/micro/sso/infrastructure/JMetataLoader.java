package jlook.micro.sso.infrastructure;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import jlook.micro.sso.ClientType;
import jlook.micro.sso.infrastructure.context.JContext;
import jlook.micro.sso.infrastructure.context.JContextException;
import jlook.micro.sso.infrastructure.context.JContextFactory;
import jlook.micro.sso.infrastructure.metadata.JDomainDef;
import jlook.micro.sso.infrastructure.metadata.JRoleDef;
import jlook.micro.sso.infrastructure.metadata.JUserDef;
import jlook.micro.sso.infrastructure.security.JUserEntity;
import jlook.micro.sso.service.JDomainService;
import jlook.micro.sso.service.JRoleService;
import jlook.micro.sso.service.JServiceException;
import jlook.micro.sso.service.JUserService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class JMetataLoader implements JLifecycle {
	private static Log LOG = LogFactory.getLog(JMetataLoader.class);
	
	@Resource(name=JContextFactory.ID)
	protected JContextFactory jContextFactory;
	
	@Resource(name=JDomainService.ID)
	private JDomainService jDomainService;
	
	@Resource(name=JRoleService.ID)
	private JRoleService jRoleService;
	
	@Resource(name=JUserService.ID)
	private JUserService jUserService;
	
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String jpaAutoCreated;
	
	protected JContext jContext;
	
	@Override
	@PostConstruct
	public void startup() throws JServiceException {
		if(LOG.isInfoEnabled()) {
			LOG.info("startup ----> "+jpaAutoCreated);
		}
		
		if("create-drop".equals(this.jpaAutoCreated)) {
			jContext = jContextFactory.getJContext();
			jContext.putClientType(ClientType.Loader.name());
			jContext.putDomainId(JDomainDef.SYSTEM_ID);
			
			// signIn
			try {
				jContext.putUserEntity(getUserEntity());
			} catch (JContextException e) {
				throw new JServiceException(e.getMessage(), e);
			}
			
			loadData();
		}
		
	}
	
	protected JUserEntity getUserEntity() {
		Long objectId 	= 1L;
		String email 	= JUserDef.admin.instance.getEmail();
		String password = JUserDef.admin.instance.getPassword();
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(JRoleDef.sysadmin.instance.getName()));
		
		JUserEntity entity = new JUserEntity( email, password, authorities);
		entity.setObjectId(objectId);
		entity.setDomainId(JUserDef.admin.instance.getDomainId());
		
		return entity;
	}
	@Override
	public void destory() {
		if(LOG.isInfoEnabled()) {
			LOG.info("destory ---->");
		}
		
	}
	
	private void loadData() throws JServiceException {
		JDomainDef[] jDomainDefs = JDomainDef.values();
    	for(JDomainDef def : jDomainDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
			jDomainService.create(def.instance);
    	}
    	
    	JRoleDef[] jRoleDefs = JRoleDef.values();
    	for(JRoleDef def : jRoleDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
			jRoleService.create(def.instance);
    	}
    	
    	JUserDef[] jUserDefs = JUserDef.values();
    	for(JUserDef def : jUserDefs) {
    		jContext.putDomainId(def.instance.getDomainId());
    		jUserService.create(def.instance);
    	}
		
	}
}
