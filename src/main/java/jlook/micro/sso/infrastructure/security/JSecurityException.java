package jlook.micro.sso.infrastructure.security;

import jlook.micro.sso.infrastructure.annotation.Message;
import jlook.micro.sso.service.JServiceException;

@Message(text=JSecurityException.TEXT)
public class JSecurityException extends JServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TEXT = "Security Error";
	
	public JSecurityException(String message) {
		super(message);
	}
	
	public  JSecurityException(Object[] parameters) {
		super(TEXT, parameters);
	}
	
	public JSecurityException(String message,Object[] parameters) {
		super(message);
	}
	
	public JSecurityException(String message, Throwable cause,Object... parameters) {
		super(message, cause, parameters);
	}
}