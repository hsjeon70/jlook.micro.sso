package jlook.micro.sso.infrastructure.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public interface JSecurityService extends UserDetailsService {
public static final String ID = "jSecurityService";
	
	public static final String SESSION_KEY = "SPRING_SECURITY_CONTEXT";
	
	public JUserEntity getUserEntity() throws JSecurityException;
	public Long getDomainId() throws JSecurityException;
	public String getClientType() throws JSecurityException;
	
	public String encodePassword(String password);
	public BCryptPasswordEncoder getPasswordEncoder();
}
