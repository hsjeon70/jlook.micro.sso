package jlook.micro.sso.infrastructure.security;

import java.io.Serializable;

public interface JBaseUser extends Serializable {
	public static final String ID="userEntity";
	public Long getObjectId();
	public void setObjectId(Long objectId);
	public Long getDomainId();
	public void setDomainId(Long domainId);
	public String getNickname();
	public void setNickname(String nickname);
	public String getUsername();
	
	// system administrator
	public boolean isSysadmin();
	// domain admin
	public boolean isAdmin();
	
} 
