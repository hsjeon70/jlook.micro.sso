package jlook.micro.sso.infrastructure.security;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService jSecurityService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.headers().frameOptions().disable();
		http.authorizeRequests().antMatchers("/", "/home").permitAll().and()
			.authorizeRequests().antMatchers("/console/**").permitAll().and()
            .authorizeRequests().antMatchers("/sayhello", "/saytest", "/test", "/mytest").permitAll().and()
            .authorizeRequests().antMatchers("/welcome", "/exception").permitAll().and()
            .authorizeRequests().antMatchers("/owners/**", "/pets/**", "/vets/**").permitAll().and()
            .authorizeRequests().antMatchers("/images/**", "/css/**").permitAll()
            //.authorizeRequests().antMatchers("/resources/**","/images/*", "/css/**", "/fragments/**", "/owners/**", "/pets/**", "/vets/**").permitAll()
            .anyRequest().authenticated();
        
        http.formLogin()
        	.defaultSuccessUrl("/home")
            .loginPage("/login")
            .permitAll()
            .and()
        .logout()
        	.logoutSuccessUrl("/login")
            .permitAll();
	}
	 
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.jSecurityService).passwordEncoder(this.jSecurityService.getPasswordEncoder());
	}
}
