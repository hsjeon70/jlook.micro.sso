package jlook.micro.sso.infrastructure.security;

public enum JRoleType {
	user("user"),
	sysadmin("sysadmin"),
	admin("admin");
	
	JRoleType(String name) {
		this.name = name;
	}
	
	private String name;
	
	public String getName() {
		return this.name;
	}
}
