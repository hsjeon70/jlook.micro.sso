package jlook.micro.sso.infrastructure.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class JUserEntity extends User implements JBaseUser {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Long objectId;
	protected Long domainId;
	protected String nickname;
	
	protected Map<Long, String> domainMap;
	
	public JUserEntity(String email, String password, Collection<GrantedAuthority> authorities) {
		super(email,password,true,true,true,true,authorities);
		this.domainMap = new HashMap<Long,String>();
	}
	
	public Long getObjectId() {
		return objectId;
	}
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
	public Long getDomainId() {
		return domainId;
	}
	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	@Override
	public boolean isSysadmin() {
		Collection<GrantedAuthority> gaList = this.getAuthorities();
		if(gaList==null) {
			return false;
		}
		GrantedAuthority gauth = new SimpleGrantedAuthority(JRoleType.sysadmin.getName());
		return gaList.contains(gauth);
	}
	
	public List<String> getRoleList() {
		List<String> roles = new ArrayList<String>();
		Collection<GrantedAuthority> gaList = this.getAuthorities();
		if(gaList==null) {
			return roles;
		}
		for(GrantedAuthority ga : gaList) {
			roles.add(ga.getAuthority());
		}
		return roles;
	}
	
	@Override
	public String toString() {
		return "JUserEntity["+super.getUsername()+"]";
	}

	@Override
	public String getNickname() {
		return this.nickname;
	}
	
	@Override
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public Map<Long,String> getDomainMap() {
		return this.domainMap;
	}
	
	public void putDomain(Long domainId, String name) {
		this.domainMap.put(domainId, name);
	}
	
	public boolean hasDomain(Long domainId) {
		return this.domainMap.containsKey(domainId);
	}
	
	public boolean hasRole(String roleName) {
		Collection<GrantedAuthority> gaList = this.getAuthorities();
		for(GrantedAuthority ga : gaList) {
			if(ga.getAuthority().equals(roleName)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String getUsername() {
		return super.getUsername();
	}

	@Override
	public boolean isAdmin() {
		Collection<GrantedAuthority> gaList = this.getAuthorities();
		if(gaList==null) {
			return false;
		}
		GrantedAuthority gauth = new SimpleGrantedAuthority(JRoleType.admin.getName());
		return gaList.contains(gauth);
	}
}