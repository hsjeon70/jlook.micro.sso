package jlook.micro.sso.infrastructure.context.impl;

import org.springframework.stereotype.Component;

import jlook.micro.sso.infrastructure.context.JContext;
import jlook.micro.sso.infrastructure.context.JContextFactory;

@Component(JContextFactory.ID)
public class JContextFactoryImpl extends JContextFactory {
	
	public JContextFactoryImpl() {
		JContextFactory.instance = this;
	}
	
	@Override
	public JContext getJContext() {
		JContextImpl jContext = new JContextImpl();
		return jContext;
	}
}