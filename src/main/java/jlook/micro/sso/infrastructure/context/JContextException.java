package jlook.micro.sso.infrastructure.context;

public class JContextException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JContextException() {
		super();
	}

	public JContextException(String message, Throwable cause) {
		super(message, cause);
	}

	public JContextException(String message) {
		super(message);
	}

	public JContextException(Throwable cause) {
		super(cause);
	}
}