package jlook.micro.sso.infrastructure.context;

import jlook.micro.sso.infrastructure.security.JBaseUser;

public interface JContext {
	public void putUserEntity(JBaseUser entity)  throws JContextException;
	public JBaseUser getUserEntity() throws JContextException;
	
	public void putDomainId(Long domainId);
	public void putClientType(String clientType);
	public Long getDomainId();
	public String getClientType();
	public String removeAll();
}