package jlook.micro.sso.service;

import jlook.micro.sso.domain.security.JRole;

public interface JRoleService {
	
	public static final String ID = "JRoleService";
	
	public void create(JRole jRole) throws JServiceException;
}
