package jlook.micro.sso.service;

import jlook.micro.sso.domain.security.JDomain;

public interface JDomainService {
	public static final String ID = "JDomainService";
	
	public void create(JDomain jDomain) throws JServiceException;
}
