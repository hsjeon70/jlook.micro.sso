package jlook.micro.sso.service;

import jlook.micro.sso.domain.security.JUser;

public interface JUserService {
	public static final String ID = "JUserService";
	
	public void create(JUser jUser) throws JServiceException;
}
