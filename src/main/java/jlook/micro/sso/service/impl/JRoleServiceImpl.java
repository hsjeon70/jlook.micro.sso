package jlook.micro.sso.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.micro.sso.domain.security.JRole;
import jlook.micro.sso.infrastructure.security.JSecurityService;
import jlook.micro.sso.infrastructure.security.JUserEntity;
import jlook.micro.sso.repository.JRoleRepository;
import jlook.micro.sso.service.JRoleService;
import jlook.micro.sso.service.JServiceException;

@Service(JRoleService.ID)
@Transactional(readOnly = true, rollbackForClassName={"jlook.framework.service.JServiceException"})
public class JRoleServiceImpl implements JRoleService {

	@Autowired
	private JRoleRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void create(JRole jRole) throws JServiceException {
		JUserEntity entity = this.security.getUserEntity();
		jRole.setCreatedBy(entity.getObjectId());
		jRole.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		this.repository.save(jRole);
	}

}
