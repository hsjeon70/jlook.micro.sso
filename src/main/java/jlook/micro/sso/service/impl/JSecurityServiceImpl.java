package jlook.micro.sso.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import jlook.micro.sso.domain.security.JRole;
import jlook.micro.sso.domain.security.JUser;
import jlook.micro.sso.infrastructure.context.JContext;
import jlook.micro.sso.infrastructure.context.JContextException;
import jlook.micro.sso.infrastructure.context.JContextFactory;
import jlook.micro.sso.infrastructure.security.JBaseUser;
import jlook.micro.sso.infrastructure.security.JSecurityException;
import jlook.micro.sso.infrastructure.security.JSecurityService;
import jlook.micro.sso.infrastructure.security.JUserEntity;
import jlook.micro.sso.repository.JUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true, rollbackForClassName={"jlook.framework.service.JServiceException"})
@Service(JSecurityService.ID)
public class JSecurityServiceImpl implements JSecurityService {
	
	@Resource(name=JContextFactory.ID)
	private JContextFactory jContextFactory;
	
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	@Autowired
	private JUserRepository jUserRepository;
	
	public static final JUserEntity ANNONYMOUS = new JUserEntity("anonymous", "", new ArrayList<GrantedAuthority>());
	
	@Override
	public JUserEntity getUserEntity() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		JBaseUser entity;
		try {
			entity = jContext.getUserEntity();
		} catch (JContextException e) {
			throw new JSecurityException("Cannot get UserEntity.",e);
		}
		
		if(entity==null) {
			return ANNONYMOUS;
		}
		if(entity instanceof JUserEntity) {
			return (JUserEntity)entity;
		}
		throw new JSecurityException("Invalid UserEntity. - "+entity);		
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		JUser jUser = this.jUserRepository.findByUsername(username);
		List<GrantedAuthority> authorities = buildUserAuthority(jUser);
		JUserEntity entity = new JUserEntity(jUser.getUsername(), jUser.getPassword(), authorities);
		entity.setObjectId(jUser.getObjectId());
		entity.setDomainId(jUser.getDomainId());
		
		JContext jContext = this.jContextFactory.getJContext();
		try {
			jContext.putUserEntity(entity);
		} catch (JContextException e) {
			throw new RuntimeException("Fail to put UserEntity to Context.",e);
		}
		return entity;
	}
	
	@Override
	public Long getDomainId() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		Long domainId = jContext.getDomainId();
		if(domainId==null) {
			throw new JSecurityException("domainId is null.");
		}
		return domainId;
	}

	@Override
	public String getClientType() throws JSecurityException {
		JContext jContext = this.jContextFactory.getJContext();
		String clientType = jContext.getClientType();
		if(clientType==null) {
			throw new JSecurityException("ClientType is null.");
		}
		return clientType;
	}
	
	private List<GrantedAuthority> buildUserAuthority(JUser jUser) {
		Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
		for(JRole role : jUser.getRoles()) {
			auths.add(new SimpleGrantedAuthority(role.getName()));
		}
		return new ArrayList<GrantedAuthority>(auths);
	}

	@Override
	public String encodePassword(String password) {
		return this.encoder.encode(password);
	}

	@Override
	public BCryptPasswordEncoder getPasswordEncoder() {
		return this.encoder;
	}
}
