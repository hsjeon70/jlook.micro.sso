package jlook.micro.sso.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.micro.sso.domain.security.JUser;
import jlook.micro.sso.infrastructure.security.JSecurityService;
import jlook.micro.sso.infrastructure.security.JUserEntity;
import jlook.micro.sso.repository.JUserRepository;
import jlook.micro.sso.service.JServiceException;
import jlook.micro.sso.service.JUserService;

@Service(JUserService.ID)
@Transactional(readOnly = true, rollbackForClassName={"jlook.framework.service.JServiceException"})
public class JUserServiceImpl implements JUserService {
	
	@Autowired
	private JUserRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void create(JUser jUser) throws JServiceException {
		JUserEntity entity = this.security.getUserEntity();
		jUser.setCreatedBy(entity.getObjectId());
		jUser.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		jUser.setPassword(security.encodePassword(jUser.getPassword()));

		this.repository.save(jUser);
	}
	
}
