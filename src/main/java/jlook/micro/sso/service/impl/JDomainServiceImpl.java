package jlook.micro.sso.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import jlook.micro.sso.domain.security.JDomain;
import jlook.micro.sso.infrastructure.security.JSecurityService;
import jlook.micro.sso.infrastructure.security.JUserEntity;
import jlook.micro.sso.repository.JDomainRepository;
import jlook.micro.sso.service.JDomainService;
import jlook.micro.sso.service.JServiceException;

@Service(JDomainService.ID)
@Transactional(readOnly = true, rollbackForClassName={"jlook.framework.service.JServiceException"})
public class JDomainServiceImpl implements JDomainService {
	
	@Autowired
	private JDomainRepository repository;
	
	@Resource(name=JSecurityService.ID)
	private JSecurityService security;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void create(JDomain jDomain) throws JServiceException {
		JUserEntity entity = this.security.getUserEntity();
		jDomain.setCreatedBy(entity.getObjectId());
		jDomain.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		
		this.repository.save(jDomain);
	}

}
