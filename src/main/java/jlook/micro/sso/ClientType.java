package jlook.micro.sso;

public enum ClientType {
	Mobile,
	Web,
	Loader,
	Batch;
}
