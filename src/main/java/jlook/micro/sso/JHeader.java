package jlook.micro.sso;

public enum JHeader {
	CLIENT(JHeader.JCLIENT_TYPE),
	DID(JHeader.JDOMAIN_ID);
	
	JHeader(String key) {
		this.key = key;
	}
	
	public static final String JCLIENT_TYPE  = "jlook.client.type";
	public static final String JDOMAIN_ID 	 = "jlook.domainId";
	public static final String JCONTENT_TYPE = "jlook.content.type";	// DEFAULT - text/html
	
	public static final String HTTP_CONTENT_TYPE = "Content-Type";
	public static final String HTTP_ACCEPT_TYPE = "Accept";
	
	private String key;
	
	public String getKey() {
		return this.key;
	}
}