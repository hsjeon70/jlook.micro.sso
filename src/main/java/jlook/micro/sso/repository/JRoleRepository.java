package jlook.micro.sso.repository;

import jlook.micro.sso.domain.security.JRole;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value=JRoleRepository.ID)
public interface JRoleRepository extends CrudRepository<JRole, Long>{
	public static final String ID = "JRoleRepository";
	
}
