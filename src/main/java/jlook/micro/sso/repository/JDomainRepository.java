package jlook.micro.sso.repository;

import jlook.micro.sso.domain.security.JDomain;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value=JDomainRepository.ID)
public interface JDomainRepository extends CrudRepository<JDomain, Long> {
	public static final String ID = "JDomainRepository";
	
}
