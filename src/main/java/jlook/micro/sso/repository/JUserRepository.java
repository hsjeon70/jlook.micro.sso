package jlook.micro.sso.repository;

import jlook.micro.sso.domain.security.JUser;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier(value=JUserRepository.ID)
public interface JUserRepository extends CrudRepository<JUser, Long>{
	public static final String ID = "JUserRepository";
	
	public JUser findByUsername(String username);
}
