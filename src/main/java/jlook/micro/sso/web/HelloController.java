package jlook.micro.sso.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {
	private static Log LOG = LogFactory.getLog(HelloController.class);
	
	@RequestMapping(value="/sayhello", method=RequestMethod.GET)
    public String sayHello() {
		if(LOG.isInfoEnabled()) {
			LOG.info("sayHello");
		}
		return "mytest";
	}
	
	@RequestMapping(value="/saytest", method=RequestMethod.GET)
    public String sayTest() {
		if(LOG.isInfoEnabled()) {
			LOG.info("sayTest");
		}
		return "test";
	}
}
